#!/usr/bin/python3
"""
backstep-traceback-auto - Automatic backup script using backstep-traceback
Copyright (c) 2010-2021, AllWorldIT

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

import argparse
import configparser
from datetime import datetime
import json
import os
import subprocess
import sys
import time

parser = argparse.ArgumentParser(
    description="Automatic backup script using backstep-traceback."
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    help="Show verbose debug information & set debug options",
)
group = parser.add_mutually_exclusive_group()
group.add_argument("-d", "--daily", action="store_true", help="Do daily backups")
group.add_argument("-f", "--failed", action="store_true", help="Do failed backups")
group.add_argument("-w", "--weekly", action="store_true", help="Do weekly backups")
parser.add_argument(
    "-l",
    "--list-failed",
    action="store_true",
    help="List lv's from last failed backup",
)
parser.add_argument(
    "-o", "--only", help="Only do a backup of the specified lv", metavar="lv_name"
)
parser.add_argument(
    "-t",
    "--test",
    action="store_true",
    help="Don't do anything, only show what would have been run",
)
args = parser.parse_args()

if not args.test:
    if not os.path.exists("/var/run/backstep-traceback-auto"):
        os.mkdir("/var/run/backstep-traceback-auto")

    JSON_FILE = "/var/run/backstep-traceback-auto/backstep-traceback-auto.json"
else:
    JSON_FILE = "backstep-traceback-auto.json"

if os.path.exists(JSON_FILE):
    with open(JSON_FILE, encoding="utf8") as json_file:
        json_dict = json.loads(json_file)

    PERIOD = json_dict["period"]
    lv_list = json_dict["lv_list"]

if args.daily:
    PERIOD = "daily"

if args.failed:
    if os.path.exists(JSON_FILE):
        os.remove(JSON_FILE)

if args.weekly:
    PERIOD = "weekly"

if not PERIOD:
    print("error: You need to specify a period [daily or weekly].")
    sys.exit(2)

if args.list_failed:
    if lv_list:
        print(json.dumps(lv_list, indent=4))
    else:
        print("error: There is no failed backup list.")

    sys.exit(0)

if not args.test:
    CONFIG_FILE = "/etc/backstep-traceback/backstep-traceback-auto.ini"
else:
    CONFIG_FILE = "backstep-traceback-auto.ini"

if not os.path.exists(CONFIG_FILE):
    print("error: backup is not configured.")
    sys.exit(2)

config_default_dict = {
    "debug_max_load": "20",
    "max_load": "8",
    "report_email": "hostmaster@iitsp.net",
    "zabbix_config": "/etc/zabbix/zabbix_agentd.conf.d/allworldit.conf",
    "server_path": "backup",
    "server_key": "/root/.ssh/backupserver_id_rsa",
}
config = configparser.ConfigParser(defaults=config_default_dict)
config.read(CONFIG_FILE)

bwlimit = config.getint(PERIOD, "bwlimit", fallback=None)
debug_max_load = config.getint(PERIOD, "debug_max_load")
max_load = config.getint(PERIOD, "max_load")
report_email = config.get(PERIOD, "report_email")
server_host = config.get(PERIOD, "server_host")
server_key = config.get(PERIOD, "server_key")

if not args.test:
    if not os.path.exists(server_key):
        print("error: " + server_key + " does not exist.")
        sys.exit(2)

server_path = config.get(PERIOD, "server_path")
server_port = config.getint(PERIOD, "server_port")
server_user = config.get(PERIOD, "server_user")
zabbix_config = config.get(PERIOD, "zabbix_config")

if not args.test:
    if not os.path.exists(zabbix_config):
        print("error: " + zabbix_config + " does not exist.")
        sys.exit(2)

if args.verbose:
    syncopts = ["--debug"]
    syncopts.append("--max-load=" + str(debug_max_load))
else:
    syncopts = ["--quiet"]
    syncopts.append("--max-load=" + str(max_load))

    if bwlimit:
        if bwlimit > 0:
            syncopts.append("--bwlimit=" + str(bwlimit))

if not lv_list:
    lv_list = json.loads(config.get(PERIOD, "backup_lvs"))

stack_dict = {}

for lv in lv_list:
    if args.only and args.only != lv:
        continue

    bind_mount_args = []
    lvm_snapshot_args = []

    if not args.test:
        if os.path.exists("/dev/lvm-raid/" + lv):
            VG = "lvm-raid"
        elif os.path.exists("/dev/lvm-cache/" + lv):
            VG = "lvm-cache"
        else:
            continue
    else:
        VG = "lvm-raid"

    if lv == "root":
        bind_mount_args.append("--bind-mount")
        bind_mount_args.append("/boot:/boot")

    lvm_snapshot_args.append("--lvm-snapshot")
    lvm_snapshot_args.append(VG + "/" + lv)

    if len(lvm_snapshot_args) > 2:
        ZABBIX_ITEM = PERIOD
    else:
        ZABBIX_ITEM = lv + "," + PERIOD

    command_line = [
        "/usr/sbin/backstep-traceback",
        "--backup",
        "--acl",
        "--safe",
        "--deltas",
        "--create-timestamp-file",
        "--copy-backup-filter",
        "--prepend-backup-filter",
    ]
    command_line.extend(lvm_snapshot_args)
    command_line.extend(bind_mount_args)
    command_line.append('--email-subject="' + PERIOD.upper() + " " + lv + '"')
    command_line.append("--email=" + report_email)
    command_line.append("--port=" + str(server_port))
    command_line.append("--pubkey=" + server_key)
    command_line.extend(syncopts)
    command_line.append("--zabbix=" + ZABBIX_ITEM)
    command_line.append("--zabbix-config=" + zabbix_config)
    command_line.append(
        "{}@{}::{}/{}-{}-{}".format(
            server_user, server_host, server_path, PERIOD, VG, lv
        )
    )

    stack_dict[lv] = command_line

#   Rsync Exit Values
#   0      Success
#   1      Syntax or usage error
#   2      Protocol incompatibility
#   3      Errors selecting input/output files, dirs
#   4      Requested action not supported
#   5      Error starting client-server protocol
#   6      Daemon unable to append to log-file
#   10     Error in socket I/O
#   11     Error in file I/O
#   12     Error in rsync protocol data stream
#   13     Errors with program diagnostics
#   14     Error in IPC code
#   20     Received SIGUSR1 or SIGINT
#   21     Some error returned by waitpid()
#   22     Error allocating core memory buffers
#   23     Partial transfer due to error
#   24     Partial transfer due to vanished source files
#   25     The --max-delete limit stopped deletions
#   30     Timeout in data send/receive
#   35     Timeout waiting for daemon connection

exit_list = [1, 2, 3, 4, 6, 13, 14, 20, 21, 22, 24, 25]
wait_list = [5, 10, 11, 12, 23, 30, 35, 255]

lv_list.reverse()

while lv_list:
    lv = lv_list.pop()

    EXIT_CODE = subprocess.call(stack_dict[lv])

    if EXIT_CODE in wait_list:
        lv_list.append(lv)

        if args.verbose:
            print("Sleeping...")

        time.sleep(3030)
    elif EXIT_CODE in exit_list:
        lv_list.append(lv)
        lv_list.reverse()

        json_dict = {}
        json_dict["datetime"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        json_dict["exitcode"] = EXIT_CODE
        json_dict["period"] = PERIOD
        json_dict["lv_list"] = lv_list

        with open(JSON_FILE, "w", encoding="utf8") as json_file:
            json.dump(json_dict, json_file, indent=4)

        sys.exit(EXIT_CODE)

sys.exit(0)
